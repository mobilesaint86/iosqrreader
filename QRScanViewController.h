//
//  QRScanViewController.h
//  Enmasses
//
//  Created by Boris on 26/04/15.
//
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import "BaseViewControler.h"
#import "LoadingView.h"

@interface QRScanViewController : BaseViewControler<ZBarReaderViewDelegate>
{
    ZBarReaderView * _reader;
    
    GrabRequestHttp* connection;
    UIImage* didDecodeImage;
    UIImageView* imgView;
    LoadingView* loadingView;
    
    ZBarReaderView * reader;
    
    float x, y, w, h;
    
    BOOL isCamera;
}

@property (nonatomic, retain) UIImage *scanImage;
@property (nonatomic, retain) NSString * strQRCode;
- (IBAction)buttonPressed:(id)sender;
@end
